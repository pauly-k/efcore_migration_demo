﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ef_core_practice.Migrations
{
    public partial class ActivityDuration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "Deativated",
                table: "Tasks",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DurationMinutes",
                table: "Activities",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Deativated",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "DurationMinutes",
                table: "Activities");
        }
    }
}
