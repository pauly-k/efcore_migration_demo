﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


namespace ef_core_practice {
    class Program {
        static void Main(string[] args) {
            var config = GetConfg();
            var connString = config["connection_string"];
            Console.WriteLine($"connection {connString}");
            EnsureDb(connString);

            Console.WriteLine("Exit");
        }

     

        static IConfiguration GetConfg() {
            var bldr = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true)
                .AddEnvironmentVariables();

            return bldr.Build();
        }

        static void EnsureDb(string connectionString) {
            var builder = new DbContextOptionsBuilder();
            builder.UseSqlServer(connectionString);

            
            using (var db = new TodoContext(builder.Options)) {
                Console.WriteLine("ensureing todo db");
                db.Database.EnsureCreated();
            }
        }
    }

  
}
