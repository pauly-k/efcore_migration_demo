namespace ef_core_practice {
	public class Todo {
		public int Id { get; set; }
		public string Title { get; set; }

		public Task Task { get; set; }
	}
}