using System;
using System.Collections.Generic;

namespace ef_core_practice {
	public class Task {
		public int Id { get; set; }
		public string Title { get; set; }
		public string Note { get; set; }
		public DateTime Created { get; set; }
		public DateTime? Deativated { get; set; }

		public List<Todo> Todos { get; set; }		
		public List<Activity> Activities { get; set; }		
	}
}