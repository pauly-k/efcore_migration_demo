

using System;

namespace ef_core_practice {
    public class Activity {
        public int Id { get; set; }
        public string Note { get; set; }
        public DateTime Start { get; set; }
        public DateTime? Stop { get; set; }
				public int DurationMinutes { get; set; }
				public DateTime Created { get; set; }
				public DateTime? Deactivated { get; set; }

				public Task Task { get; set; }
    }
}