using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;


namespace ef_core_practice {
    public class TodoContext : DbContext {

        private readonly string _connStr;
        public TodoContext(string connStr) : base() {
            this._connStr = connStr;
        }

        public TodoContext(DbContextOptions options)
        : base(options) {
            Console.WriteLine("options aready provided");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            Console.WriteLine("configuratin todo context");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {

            // todos
            modelBuilder.Entity<Todo>()
                .HasIndex(t => t.Title);

            modelBuilder.Entity<Todo>()
                .Property(t => t.Title)
                .HasColumnName("Title")
                .HasMaxLength(250)
                .IsRequired();                

            // tasks
            modelBuilder.Entity<Task>()
                .HasIndex(t => t.Title);

            modelBuilder.Entity<Task>()
                .Property(t => t.Title)
                .HasColumnName("Title")
                .HasMaxLength(250)
                .IsRequired();

            // activity
            var activityEntity = modelBuilder.Entity<Activity>();
                
            activityEntity.HasIndex(t => t.Start);
            activityEntity.HasIndex(t => t.Created);
            activityEntity.HasOne(t => t.Task)
                .WithMany(t => t.Activities);
                
        }


        public DbSet<Todo> Todos { get; set; }

        public DbSet<Task> Tasks { get; set; }
        public DbSet<Activity> Activities { get; set; }
    }
}