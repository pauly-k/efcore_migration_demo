using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace ef_core_practice {
	public class TodoContextFactory : IDesignTimeDbContextFactory<TodoContext>
    {
        public TodoContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<TodoContext>();
            optionsBuilder.UseSqlServer("server=localhost;database=todo;uid=sa;pwd='Stopangry-8'");

            return new TodoContext(optionsBuilder.Options);
        }
    }
}